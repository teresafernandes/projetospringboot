/********** INICIO CONSULTA DE FÃ CLUBE **********/
function carregarCidadesPorEstadoConsultaFaClube() {
    var url = '/cidades';
    
    if (document.getElementById("selectEstado_consulta_faclube").value != '') {
        url =  '/' + document.getElementById("selectEstado_consulta_faclube").value + url ;
    }
    
    $("#selectCidade_consulta_faclube_div").load(url);
}

function buscarFaClubes() {

    var url = '';
    
    if (document.getElementById("selectCidade_consulta_faclube").value > 0) {
        url = '/cidade/' + document.getElementById("selectCidade_consulta_faclube").value + url ;
    }else if (document.getElementById("selectEstado_consulta_faclube").value > 0) {
    		url = '/estado/' + document.getElementById("selectEstado_consulta_faclube").value + url ;
    }
    
    if (document.getElementById("selectArtista_consulta_faclube").value > 0) {
    	url = url+ '/artista/'+ document.getElementById("selectArtista_consulta_faclube").value ;
    }
    
    url += '/faclubes';
    
    $("#lista_faclubes").load(url);
    

}
/********** FIM CONSULTA DE FÃ CLUBE **********/

/********** INICIO CADASTRO DE FÃ CLUBE **********/
function cadastrarFaClube() {
  alert('Antes de concluir o cadastro, confirme se os dados foram informados corretamente! \nAssim que cadastrado, o Fã Clube será imediatamente disponibilisado para consulta neste site.');
  $.ajax({
    url: "/faclubes/cadastro",
    type: "POST",
    enctype: 'multipart/form-data',
    data: new FormData($("#cadastro-faclube-form")[0]),
    processData: false,
    contentType: false,
    cache: false,
    success: function (data, textStatus, xhr) {
    	$("#div-mensagem-cadastro-faclube").css("display", "block");
    	$("#mensagem-cadastro-faclube").text(data.mensagem);
	    $("#div-mensagem-cadastro-faclube").addClass("alert-success");
	    $("#div-mensagem-cadastro-faclube").removeClass("alert-error");
	    updateFaClubeCadastrado(data);
    },
    error: function (data, textStatus, xhr) {
    	var dataObj = JSON.parse(data.responseText);
    	$("#div-mensagem-cadastro-faclube").css("display", "block");
	    $("#mensagem-cadastro-faclube").html(dataObj.mensagem == null ? (dataObj.message == null ? "ERRO!" : dataObj.message) : dataObj.mensagem);
	    $("#div-mensagem-cadastro-faclube").addClass("alert-error");
	    $("#div-mensagem-cadastro-faclube").removeClass("alert-success");
    }
  });
} 

function updateFaClubeCadastrado(data){
	 $("#nome_fc").val(data.nome);
	 $("#descricao_fc").val(data.descricao);
	 $("#email_fc").val(data.email);
	 $("#telefone_fc").val(data.telefone);
	 $("#twitter_fc").val(data.twitter);
	 $("#insta_fc").val(data.instagram);
	 $("#face_fc").val(data.facebook);
	 $("#site_fc").val(data.site);
	 $("#representantes_fc").val(data.representantes);
	 $("#outrasredes_fc").val(data.outrsRedesSociais);
	 $("#data_fc").val(data.dataDeCriacao);
	 $("#selectEstado_cadastro_faclube").val(null);
	 $("#selectEstado_cadastro_faclube").css("color", "#7F8289");
	 $("#selectCidade_cadastro_faclube").css("color", "#7F8289");
	 $("#selectCidade_cadastro_faclube").val(data.cidade);
	 $("#logo_fc").val(null);
	 $("#artista_fc").val(data.artista);
	 $("#idartista_fc").val(null);
}

function carregarCidadesPorEstadoCadastroFaClube() {
    var url = '/cidades';
    
    if (document.getElementById("selectEstado_cadastro_faclube").value != '') {
        url =  '/faclubes/cadastro/' + document.getElementById("selectEstado_cadastro_faclube").value + url ;
    }
    
    $("#selectCidade_cadastro_faclube_div").load(url);
}


$("#artista_fc").autocomplete({
	source: function(request, response) {
		$.ajax({
           url : "/artistas",
           type: "POST",
           data: new FormData($("#cadastro-faclube-form")[0]),
           processData: false,
           contentType: false,
           cache: false,
           success : function(data) {
        	   response(jQuery.map(data, function(item) {
                   return {
                      label: item.nome,
                      value: item.id
                   }
              }))
           },
           error: function () {
	      }
		});
	},
    select: function (event, ui) {
        $("#idartista_fc").val(ui.item.value); // display the selected text
        return false;
    }
});
/********** FIM CADASTRO DE FÃ CLUBE **********/


function mudarCor(select){
	if ($("#"+select.id+" option[value='0']")[0].selected) {
		$(select).css("color", "#7F8289");
	}else{
		$(select).css("color", "white");
	}
}

$( "#logo_fc" ).change(function() { 
	 var output = document.getElementById('output-imagem-logo');
	 output.src = URL.createObjectURL(event.target.files[0]);
});
