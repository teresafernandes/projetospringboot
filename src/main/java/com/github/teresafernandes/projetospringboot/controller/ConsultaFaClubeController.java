package com.github.teresafernandes.projetospringboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.teresafernandes.minhaarquitetura.dominio.Cidade;
import com.github.teresafernandes.minhaarquitetura.service.CidadeService;
import com.github.teresafernandes.projetospringboot.dominio.CidadeArtista;
import com.github.teresafernandes.projetospringboot.dominio.FaClube;
import com.github.teresafernandes.projetospringboot.service.FaClubeService;

@Controller
public class ConsultaFaClubeController {

	@Autowired
	private CidadeService cidadeService;
	@Autowired
	private FaClubeService faClubeService;
	
	@RequestMapping(value = {"/cidade/{cidadeId}/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubesPorCidade(Model model, @PathVariable("cidadeId") Integer cidadeId) {
		if(cidadeId != null){
			List<FaClube> fcs = faClubeService.findByCidadeId(cidadeId);
			processarListaFaClubes(fcs);
		
			model.addAttribute("listaFaClubes", fcs);
		}
		
		
	    return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}
	
	@RequestMapping(value = {"/estado/{estadoId}/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubesPorEstado(Model model, @PathVariable("estadoId") Integer estadoId) {
		if(estadoId != null){
			List<FaClube> fcs = faClubeService.findByEstadoId(estadoId);
			processarListaFaClubes(fcs);
		
			model.addAttribute("listaFaClubes", fcs);
		}
		
		
	    return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}
	
	@RequestMapping(value = {"/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubes(Model model) {
		model.addAttribute("listaFaClubes", new ArrayList<FaClube>());		
		return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}
	
	@RequestMapping(value = {"/{estadoId}/cidades"}, method = RequestMethod.GET)
	public String buscarCidadesPorEstado(Model model, @PathVariable("estadoId") Integer estadoId) {
		model.addAttribute("listaCidades", getCidadesByEstadoId(estadoId));
		model.addAttribute("faclubeCadastrado",  new FaClube());
		return "fragments/_include_consulta_faclube :: selectCidade_consulta_faclube_fragment";
	}
	
	@RequestMapping(value = {"/cidades"}, method = RequestMethod.GET)
	public String buscarCidades(Model model) {
		model.addAttribute("listaCidades", getCidades());
		return "fragments/_include_consulta_faclube :: selectCidade_consulta_faclube_fragment";
	}
	
	@RequestMapping(value = {"/artista/{artistaId}/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubesPorArtista(Model model, @PathVariable("artistaId") Integer artistaId) {
		if(artistaId != null){
			List<FaClube> fcs = faClubeService.findByArtistaId(artistaId);
			processarListaFaClubes(fcs);
			model.addAttribute("listaFaClubes", fcs);
		}
		
		return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}

	@RequestMapping(value = {"/cidade/{cidadeId}/artista/{artistaId}/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubesPorCidadeEArtista(Model model, @PathVariable("cidadeId") Integer cidadeId, @PathVariable("artistaId") Integer artistaId) {
		if(cidadeId != null && artistaId != null){
			List<FaClube> fcs = faClubeService.findByCidadeIdAndArtistaId(cidadeId, artistaId);
			processarListaFaClubes(fcs);		
			model.addAttribute("listaFaClubes", fcs);
		}		
		
	    return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}
	
	@RequestMapping(value = {"/estado/{estadoId}/artista/{artistaId}/faclubes"}, method = RequestMethod.GET)
	public String buscarFaClubesPorEstadoEArtista(Model model, @PathVariable("estadoId") Integer estadoId, @PathVariable("artistaId") Integer artistaId) {
		if(estadoId != null && artistaId!=null){
			List<FaClube> fcs = faClubeService.findByEstadoIdAndArtistaId(estadoId, artistaId);
			processarListaFaClubes(fcs);
			model.addAttribute("listaFaClubes", fcs);
		}		
		
	    return "fragments/_include_lista_faclube :: _include_lista_faclube";
	}
	
	private void processarListaFaClubes(List<FaClube> lista){
		for(FaClube f : lista){
			if(f.getLogo()!=null && f.getLogo().length > 0)
				f.setLogoBase64(Base64.encodeBase64String(f.getLogo()));
		}
	}
	
	public List<Cidade> getCidadesByEstadoId(Integer id) {
		return this.cidadeService.findByEstadoId(id);
	}
	
	public List<Cidade> getCidades() {
		return this.cidadeService.findAll();
	}	

	@ModelAttribute("cidadeSelecionada")
	public CidadeArtista prepareCidadeModel() {
	    return new CidadeArtista();
	}
	

}
