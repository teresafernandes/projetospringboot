package com.github.teresafernandes.projetospringboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.teresafernandes.minhaarquitetura.dominio.Estado;
import com.github.teresafernandes.minhaarquitetura.service.EstadoService;
import com.github.teresafernandes.projetospringboot.dominio.Artista;
import com.github.teresafernandes.projetospringboot.dominio.CidadeArtista;
import com.github.teresafernandes.projetospringboot.dominio.FaClube;
import com.github.teresafernandes.projetospringboot.service.ArtistaService;
import com.github.teresafernandes.projetospringboot.service.FaClubeService;

@Controller
public class IndexController {

	@Autowired
	private EstadoService estadoService;
	@Autowired
	private FaClubeService faClubeService;
	@Autowired
	private ArtistaService artistaService;
	
	@RequestMapping(value={"/","/index"}, method = RequestMethod.GET)
	public String carregarPaginaPrincipal(Model model) {
	    model.addAttribute("cidadeSelecionada", new CidadeArtista());
	    model.addAttribute("faclubeCadastrado", new FaClube());
	    model.addAttribute("listaArtistas", prepareListaArtistas());
	    return "index";
	}
	
	@ModelAttribute("listaEstados")
	public List<Estado> getEstados() {
		return this.estadoService.findAll();
	}	

	@ModelAttribute("listaArtistas")
	public List<Artista> prepareListaArtistas() {
	    return artistaService.findAll();
	}
	
}
