package com.github.teresafernandes.projetospringboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;
import com.github.teresafernandes.minhaarquitetura.service.CidadeService;
import com.github.teresafernandes.projetospringboot.dominio.Artista;
import com.github.teresafernandes.projetospringboot.dominio.FaClube;
import com.github.teresafernandes.projetospringboot.service.ArtistaService;
import com.github.teresafernandes.projetospringboot.service.FaClubeService;

@Controller
public class CadastroFaClubeController {
	

	@Autowired
	private CidadeService cidadeService;
	@Autowired
	private FaClubeService faClubeService;
	@Autowired
	private ArtistaService artistaService;
	

	@ModelAttribute("faclubeCadastrado")
	public FaClube prepareFaClubeModel() {
	    return new FaClube();
	}
	
	@ModelAttribute("listaArtistas")
	public List<Artista> prepareListaArtistas() {
	    return artistaService.findAll();
	}
	
	@RequestMapping(value = "/faclubes/cadastro", method = RequestMethod.POST)
	@ResponseBody
    public ResponseEntity<FaClube> cadastrarFaClube( @ModelAttribute(value="faclubeCadastrado") FaClube faclube, BindingResult br) {
		try{
			faClubeService.create(faclube);
			faclube = prepareFaClubeModel();
			faclube.setMensagem("Fã clube cadastrado com sucesso!");
			br.getModel().put("listaArtistas", prepareListaArtistas());
			return new ResponseEntity<FaClube>(faclube,HttpStatus.OK);
		}catch(ValidationException e){
			faclube.setMensagem(e.getMessage());
			faclube.setArquivoLogo(null);
			return new ResponseEntity<FaClube>(faclube,HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = {"/faclubes/cadastro/{estadoId}/cidades"}, method = RequestMethod.GET)
	public String buscarCidadesPorEstado(Model model, @PathVariable("estadoId") Integer estadoId) {
		model.addAttribute("listaCidades", cidadeService.findByEstadoId(estadoId));
		model.addAttribute("faclubeCadastrado",  new FaClube());
		return "fragments/_include_cadastro_faclube :: selectCidade_cadastro_faclube_fragment";
	}
	
	@RequestMapping(value = "/artistas", method = RequestMethod.POST)
	@ResponseBody
    public List<Artista> autocompleteArtista( @ModelAttribute(value="faclubeCadastrado") FaClube faclube, BindingResult br) {
		return artistaService.findByNomeLikeIgnoreCase(faclube.getArtista().getNome());
    }
}
