package com.github.teresafernandes.projetospringboot.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EntityScan(basePackages={"com.github.teresafernandes.minhaarquitetura.dominio", "com.github.teresafernandes.projetospringboot.dominio"})
@ComponentScan(basePackages={"com.github.teresafernandes.minhaarquitetura.service","com.github.teresafernandes.projetospringboot.service"})
@EnableJpaRepositories(basePackages = {"com.github.teresafernandes.minhaarquitetura.repository","com.github.teresafernandes.projetospringboot.repository"}) 
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index").setViewName("/index");
    }
	
}
