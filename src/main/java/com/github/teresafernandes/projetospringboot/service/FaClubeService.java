package com.github.teresafernandes.projetospringboot.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;
import com.github.teresafernandes.minhaarquitetura.util.StringUtil;
import com.github.teresafernandes.minhaarquitetura.util.ValidatorUtil;
import com.github.teresafernandes.projetospringboot.dominio.FaClube;
import com.github.teresafernandes.projetospringboot.repository.FaClubeRepository;

@Service
public class FaClubeService {

	@Autowired
	private FaClubeRepository faClubeRepository;
	@Autowired
	private ArtistaService artistaService;
	
	public List<FaClube> findAll(){
		return faClubeRepository.findAll();
	}
	
	public FaClube findById(final Integer id){
		return faClubeRepository.findById(id);
	}
	
	public List<FaClube> findByCidadeId(final Integer id){
		return (List<FaClube>) faClubeRepository.findByCidadeId(id);
	}
	
	public List<FaClube> findByEstadoId(final Integer id){
		return (List<FaClube>) faClubeRepository.findByEstadoId(id);
	}
	
	public List<FaClube> findByArtistaId(Integer artistaId){
		return (List<FaClube>) faClubeRepository.findByArtistaId(artistaId);
	}
	public List<FaClube> findByEstadoIdAndArtistaId(Integer estadoId, Integer artistaId){
		return (List<FaClube>) faClubeRepository.findByEstadoIdAndArtistaId(estadoId, artistaId);
	}
	public List<FaClube> findByCidadeIdAndArtistaId(Integer cidadeId, Integer artistaId){
		return (List<FaClube>) faClubeRepository.findByCidadeIdAndArtistaId(cidadeId, artistaId);
	}
	
	
	public void create(FaClube faclube) throws ValidationException{
		validate(faclube);	
		try {
			faclube.setLogo(faclube.getArquivoLogo().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(ValidatorUtil.isEmpty(faclube.getArtista().getId())){
			faclube.setArtista(artistaService.create(faclube.getArtista()));
		}
		faClubeRepository.save(faclube);
	}
	
	private void validate(FaClube faclube) throws ValidationException{
		List<String> erros = new ArrayList<String>();
		
		if(faclube.getArtista() == null){
			ValidatorUtil.validate("Artista", faclube.getArtista(),erros);
		}else{
			if(ValidatorUtil.isEmpty(faclube.getArtista().getId())){
				ValidatorUtil.validate("Artista", faclube.getArtista().getNome(),erros);
			}else{
				ValidatorUtil.validate("Artista", faclube.getArtista().getId(),erros);
			}
		}

		ValidatorUtil.validate("Nome", faclube.getNome(),erros);
		ValidatorUtil.validate("Email", faclube.getEmail(),erros);
		ValidatorUtil.validate("Estado", faclube.getEstado(),erros);
		ValidatorUtil.validate("Cidade", faclube.getCidade(),erros);
		
		if(!erros.isEmpty())
			throw new ValidationException(StringUtil.listaToString(erros));
	}
}
