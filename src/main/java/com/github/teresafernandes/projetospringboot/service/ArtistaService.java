package com.github.teresafernandes.projetospringboot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;
import com.github.teresafernandes.minhaarquitetura.util.StringUtil;
import com.github.teresafernandes.minhaarquitetura.util.ValidatorUtil;
import com.github.teresafernandes.projetospringboot.dominio.Artista;
import com.github.teresafernandes.projetospringboot.repository.ArtistaRepository;

@Service
public class ArtistaService {

	@Autowired
	private ArtistaRepository artistaRepository;
	
	public List<Artista> findAll(){
		return artistaRepository.findAll();
	}
	
	public Artista findById(final Integer id){
		return artistaRepository.findById(id);
	}
	
	public List<Artista> findByNome(final String nome){
		return (List<Artista>) artistaRepository.findByNome(nome);
	}
	
	public List<Artista> findByNomeLikeIgnoreCase(final String nome){
		if(ValidatorUtil.isNotEmpty(nome))
			return (List<Artista>) artistaRepository.findByNomeLikeIgnoreCase("%"+nome+"%");
		return null;
	}	
	
	public Artista create(Artista artista) throws ValidationException{
		validate(artista);	
		return artistaRepository.save(artista);
	}
	
	private void validate(Artista artista) throws ValidationException{
		List<String> erros = new ArrayList<String>();
		
		ValidatorUtil.validate("Nome", artista.getNome(),erros);
		if(!erros.isEmpty())
			throw new ValidationException(StringUtil.listaToString(erros));
	}
}
