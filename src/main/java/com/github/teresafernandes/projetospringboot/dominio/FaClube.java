package com.github.teresafernandes.projetospringboot.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.teresafernandes.minhaarquitetura.dominio.Cidade;
import com.github.teresafernandes.minhaarquitetura.dominio.EntidadeId;
import com.github.teresafernandes.minhaarquitetura.dominio.Estado;
import com.github.teresafernandes.minhaarquitetura.dominio.MensagemValidacao;
import com.github.teresafernandes.minhaarquitetura.util.StringUtil;

@Entity
public class FaClube extends MensagemValidacao implements EntidadeId{

    @Id
    @Column(name="id_faclube")
    @GeneratedValue
    private Integer id;

    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_cidade")
    private Cidade cidade;

    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_estado")
    private Estado estado;
    
    @Column(name="nome")
    private String nome;
    
    @Column(name="descricao")
    private String descricao;
    
    @Column(name="data_de_criacao")
	private Date dataDeCriacao;
	
    @Lob
    @Column(name="logo")
    private byte[] logo;
    
    @Transient
    private MultipartFile arquivoLogo;
    
    @Transient
    private String logoBase64;
    
    @Column(name="representantes")
	private String representantes;
    
    @Column(name="twitter")
	private String twitter;
    
    @Column(name="facebook")
	private String facebook;
    
    @Column(name="instagram")
	private String instagram;
    
    @Column(name="outras_redes_sociais")
	private String outrasRedesSociais;
    
    @Column(name="site")
	private String site;
    
    @Column(name="email")
	private String email;
    
    @Column(name="telefone")
	private String telefone;
    
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_artista")
    private Artista artista;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataDeCriacao() {
		return dataDeCriacao;
	}

	public void setDataDeCriacao(Date dataDeCriacao) {
		this.dataDeCriacao = dataDeCriacao;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public MultipartFile getArquivoLogo() {
		return arquivoLogo;
	}

	public void setArquivoLogo(MultipartFile arquivoLogo) {
		this.arquivoLogo = arquivoLogo;
	}

	public String getRepresentantes() {
		return representantes;
	}

	public void setRepresentantes(String representantes) {
		this.representantes = representantes;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public String getOutrasRedesSociais() {
		return outrasRedesSociais;
	}

	public void setOutrasRedesSociais(String outrasRedesSociais) {
		this.outrasRedesSociais = outrasRedesSociais;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public FaClube(){ this.cidade = new Cidade(); this.estado = new Estado(); this.artista = new Artista();}
    
    public FaClube(Cidade cidade, String nome){
    	this.cidade = cidade;
    	this.nome = nome;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FaClube other = (FaClube) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public String getLogoBase64() {
		return logoBase64;
	}

	public void setLogoBase64(String logoBase64) {
		this.logoBase64 = logoBase64;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		List<String> tostring = new ArrayList<String>();
		StringUtil.parseToString("Artista", artista!=null ? artista.getNome() : "", tostring);
		StringUtil.parseToString("Descrição", descricao, tostring);
		StringUtil.parseToString("Representantes", representantes, tostring);
		StringUtil.parseToString("E-mail", email, tostring);
		StringUtil.parseToString("Telefone", telefone, tostring);
		StringUtil.parseToString("Instagram", instagram, tostring);
		StringUtil.parseToString("Facebook", facebook, tostring);
		StringUtil.parseToString("Twitter", twitter, tostring);
		StringUtil.parseToString("Outras redes", outrasRedesSociais, tostring);
		StringUtil.parseToString("Site", site, tostring);
		StringUtil.parseToString("Estado", estado.getNome(), tostring);
		StringUtil.parseToString("Cidade", cidade.getNome(), tostring);
		
		return StringUtil.listaToString(tostring);
	}

	public Artista getArtista() {
		return artista;
	}

	public void setArtista(Artista artista) {
		this.artista = artista;
	}

}
