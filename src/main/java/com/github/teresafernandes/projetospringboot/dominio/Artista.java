package com.github.teresafernandes.projetospringboot.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.github.teresafernandes.minhaarquitetura.dominio.EntidadeId;
import com.github.teresafernandes.minhaarquitetura.dominio.MensagemValidacao;

@Entity
public class Artista extends MensagemValidacao implements EntidadeId{

    @Id
    @Column(name="id_artista")
    @GeneratedValue
    private Integer id;
    
    @Column(name="nome")
    private String nome;
    
    @Column(name="banda")
    private String banda;
    
    @Column(name="sexo_feminino")
	private boolean sexoFeminino;
	
    @Column(name="profissao")
	private String profissao;
    
    @Column(name="cantor")
    private boolean cantor;
    
    @Column(name="estilo_musical")
    private String estiloMusical;   
    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getBanda() {
		return banda;
	}

	public void setBanda(String banda) {
		this.banda = banda;
	}

	public boolean isSexoFeminino() {
		return sexoFeminino;
	}

	public void setSexoFeminino(boolean sexoFeminino) {
		this.sexoFeminino = sexoFeminino;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public boolean isCantor() {
		return cantor;
	}

	public void setCantor(boolean cantor) {
		this.cantor = cantor;
	}

	public String getEstiloMusical() {
		return estiloMusical;
	}

	public void setEstiloMusical(String estiloMusical) {
		this.estiloMusical = estiloMusical;
	}
	
	public Artista() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artista other = (Artista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
