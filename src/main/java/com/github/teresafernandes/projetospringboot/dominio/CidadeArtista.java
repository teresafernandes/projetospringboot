package com.github.teresafernandes.projetospringboot.dominio;

import javax.persistence.Transient;

import com.github.teresafernandes.minhaarquitetura.dominio.Cidade;

public class CidadeArtista  extends Cidade{


    
    @Transient
    private Artista artista;

	
    public CidadeArtista(){}


	public Artista getArtista() {
		return artista;
	}


	public void setArtista(Artista artista) {
		this.artista = artista;
	}
    
    
    

}
