package com.github.teresafernandes.projetospringboot.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.github.teresafernandes.projetospringboot.dominio.FaClube;

public interface FaClubeRepository  extends JpaRepository<FaClube, Long> {
	Collection<FaClube> findByCidadeId(Integer cidadeId);
	Collection<FaClube> findByEstadoId(Integer estadoId);
	Collection<FaClube> findByArtistaId(Integer artistaId);
	Collection<FaClube> findByEstadoIdAndArtistaId(Integer estadoId, Integer artistaId);
	Collection<FaClube> findByCidadeIdAndArtistaId(Integer cidadeId, Integer artistaId);
	FaClube findById(Integer faclubeId);
}
