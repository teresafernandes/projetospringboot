package com.github.teresafernandes.projetospringboot.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.github.teresafernandes.projetospringboot.dominio.Artista;

public interface ArtistaRepository  extends JpaRepository<Artista, Long> {
	Collection<Artista> findByNome(String nome);
	Collection<Artista> findByNomeLikeIgnoreCase(String nome);
	Artista findById(Integer artistaId);
}
